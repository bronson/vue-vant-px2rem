// 本地环境配置
module.exports = {
  baseURL: 'http://127.0.0.1:9527', // 本地项目地址
  baseAPI: 'https://api.qiuziying.top/api' // 本地api请求地址
}
