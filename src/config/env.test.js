// 测试环境配置
module.exports = {
  baseURL: 'https://test.qiuziying.top', // 项目测试地址
  baseAPI: 'https://api.qiuziying.top/api' // api请求地址
}
